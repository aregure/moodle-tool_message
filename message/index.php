<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   tool_message
 * @author    Sample Author
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__. '/../../../config.php');

global $DB;

$PAGE->set_url(new moodle_url('/admin/tool/message/index.php'));
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('manage', 'tool_message'));
$PAGE->set_pagelayout('admin');

$activeuser = $DB->get_record('user', ['id' => $USER->id]);
$messages = $DB->get_records('local_message');

echo $OUTPUT->header();

$templatecontext = (object)[
    'header' => get_string('msg_header', 'tool_message'),
    'subheader1' => get_string('msg_subheader1', 'tool_message') . $activeuser->firstname . " " . $activeuser->lastname,
    'subheader2' => get_string('msg_subheader2', 'tool_message'),
    'todayis' => get_string('today', 'tool_message') . date('F d, Y'),
    'messages' => array_values($messages),
    'editurl' => new moodle_url('/admin/tool/message/edit.php'),
];

echo $OUTPUT->render_from_template('tool_message/manage', $templatecontext);

echo $OUTPUT->footer();