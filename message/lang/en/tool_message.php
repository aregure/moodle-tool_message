<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_message', language 'en'
 *
 * @package   tool_message
 * @copyright Sample Author
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['enter_message'] = 'Please enter a message';
$string['msg_header'] = 'Manage Messages';
$string['msg_subheader1'] = 'Hello, ';
$string['msg_subheader2'] = 'You can manage messages here.';
$string['today'] = 'Today is ';
$string['message_type'] = 'Message type:';
$string['message_text'] = 'Message text:';
$string['edit_message'] = 'Edit Messages';
$string['cancelled'] = 'Message edit has been cancelled.';
$string['success'] = 'Message created successfully!';
$string['manage'] = 'Manage Messages';
$string['extpagename'] = 'Manage messages';