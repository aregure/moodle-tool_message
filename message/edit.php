<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   tool_message
 * @author    Sample Author
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__. '/../../../config.php');
require_once($CFG->dirroot . '/admin/tool/message/classes/form/edit.php');

global $DB;

$PAGE->set_url(new moodle_url('/admin/tool/message/edit.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title(get_string('edit_message', 'tool_message'));


$mform = new edit();

if ($mform->is_cancelled()) {
    // return to message management
    redirect($CFG->wwwroot . '/admin/tool/message/index.php', get_string('cancelled', 'tool_message'));
} else if ($fromform = $mform->get_data()) {
    // insert data to mdl_local_message DB table
    $recordtoinsert = new stdClass();
    $recordtoinsert->messagetext = $fromform->messagetext;
    $recordtoinsert->messagetype = $fromform->messagetype;

    $DB->insert_record('local_message', $recordtoinsert);
    redirect($CFG->wwwroot . '/admin/tool/message/index.php', get_string('success', 'tool_message') . ' Title: \'' . $fromform->messagetext . '\', type ' . $fromform->messagetype);
}

echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();