<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
/**
 * @package   tool_message
 * @author    Sample Author
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function tool_message_before_standard_top_of_body_html() {
    global $DB, $USER;
    if ($USER->id != 0 && $USER->id != 2) {
        global $DB, $USER;
        $messages = $DB->get_records('local_message');
        
        $query2 = "SELECT id, messagetext, messagetype
                    FROM {local_message}
                    WHERE id NOT IN (
                        SELECT lm.id
                        FROM {local_message} lm
                        LEFT OUTER JOIN {local_message_read} lmr
                        ON lm.id = lmr.messageid
                        WHERE lmr.userid = :userid
                    )";

        $params = [
            'userid' => $USER->id,
        ];

        $messages = $DB->get_records_sql($query2, $params);

        foreach ($messages as $message) {
            $type = \core\output\notification::NOTIFY_INFO;
            switch($message->messagetype) {
                case '0':
                    $type = \core\output\notification::NOTIFY_WARNING;
                    break;
                case '1':
                    $type = \core\output\notification::NOTIFY_SUCCESS;
                    break;
                case '2':
                    $type = \core\output\notification::NOTIFY_ERROR;
                    break;
                case '3':
                    $type = \core\output\notification::NOTIFY_INFO;
                    break;
                default:
                    $type = \core\output\notification::NOTIFY_INFO;
            }

            \core\notification::add($message->messagetext, $type);

            $readrecord = new stdClass();
            $readrecord->messageid = $message->id;
            $readrecord->userid = $USER->id;
            $readrecord->timeread = time();
            $DB->insert_record('local_message_read', $readrecord);
        }
    }
}

