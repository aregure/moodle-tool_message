# moodle-tool_message

## What does it do?
This is a Moodle plugin that allows admins to send announcement notifications to users. Notifications are stored in a database. Notifications disappear once the user sees it for the first time.

## How to install
1. Copy the `message` folder to your moodle/admin/tool folder.
2. Upgrade your moodle install.
3. Log in to moodle as admin, and go to "Site Administration", and there should be a new setting that says "Manage messages"